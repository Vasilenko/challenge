package com.urancompany.challenge.utils;

import java.text.SimpleDateFormat;

/**
 * Created by Deni on 18.04.2016.
 */
public class Const {
    public static final String YES = "yes";
    public static final String NO = "no";
    public static final String PARAM_PARENT_ID = "parent_id";
    public static final SimpleDateFormat FORMATTER = new SimpleDateFormat("MM,dd,YY");
}
