package com.urancompany.challenge.presenter;

import com.urancompany.challenge.model.FileModel;

import java.util.List;

/**
 * Created by Deni on 18.04.2016.
 */
public interface FilesPresenter {
    void loadData(int parentId);

    void onGetData(List<FileModel> list);
}
