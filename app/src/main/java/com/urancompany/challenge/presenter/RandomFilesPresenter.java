package com.urancompany.challenge.presenter;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;

import com.urancompany.challenge.model.FileModel;
import com.urancompany.challenge.model.db.DatabaseHelperFactory;
import com.urancompany.challenge.model.db.FilesDao;
import com.urancompany.challenge.model.interactors.FilesInteractor;
import com.urancompany.challenge.model.interactors.RandomFilesInteractor;
import com.urancompany.challenge.model.loaders.FilesLoader;
import com.urancompany.challenge.view.FileTableView;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Deni on 18.04.2016.
 */
public class RandomFilesPresenter implements FilesPresenter {
    private FileTableView mView;
    private FilesInteractor mInteractor;
    private Context mContext;
    private static final int LOADER_ID = 1;

    public RandomFilesPresenter(AppCompatActivity context, FileTableView view) {
        mContext = context;
        mView = view;
        mInteractor = new RandomFilesInteractor();

        context.getSupportLoaderManager().initLoader(LOADER_ID, null, new FilesCallback());
    }

    private void saveToDatabaseAsync(final List<FileModel> list) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    FilesDao dao = DatabaseHelperFactory.getHelper().getFileModelDao();
                    dao.deleteBuilder().delete();
                    dao.bulkInsert(list);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void loadData(int parentId) {
        if (parentId == -1) {
            List<FileModel> list = mInteractor.getFiles();
            saveToDatabaseAsync(list);
        } else {
            try {
                List<FileModel> list = DatabaseHelperFactory.getHelper().getFileModelDao().getList(parentId);
                onGetData(list);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onGetData(List<FileModel> list) {
        mView.showData(list);
    }

    private class FilesCallback implements LoaderManager.LoaderCallbacks<List<FileModel>> {

        @Override
        public Loader<List<FileModel>> onCreateLoader(int id, Bundle args) {
            return new FilesLoader(mContext, DatabaseHelperFactory.getHelper());
        }

        @Override
        public void onLoadFinished(Loader<List<FileModel>> loader, List<FileModel> data) {
            if (data != null) {
                onGetData(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<List<FileModel>> loader) {

        }
    }
}
