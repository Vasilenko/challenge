package com.urancompany.challenge.model.enums;

/**
 * Created by Deni on 18.04.2016.
 */
public enum FileType {
    PDF, IMAGE, VIDEO, AUDIO;
}
