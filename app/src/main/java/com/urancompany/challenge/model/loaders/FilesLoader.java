package com.urancompany.challenge.model.loaders;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.AsyncTaskLoader;

import com.urancompany.challenge.model.FileModel;
import com.urancompany.challenge.model.db.DatabaseHelper;

import java.sql.SQLException;
import java.util.List;

public class FilesLoader extends AsyncTaskLoader<List<FileModel>> {
    public static final String RELOAD = FileModel.class.getSimpleName() + ".RELOAD";
    private TestObjObserver mObserver;
    private DatabaseHelper mDatabaseHelper;

    public FilesLoader(Context context, DatabaseHelper databaseHelper) {
        super(context);
        mDatabaseHelper = databaseHelper;
    }

    @Override
    protected void onStartLoading() {

        if (mObserver == null) {
            mObserver = new TestObjObserver(this);
        }

        forceLoad();
        super.onStartLoading();
    }

    @Override
    public List<FileModel> loadInBackground() {
        try {
            return mDatabaseHelper.getFileModelDao().getList();
        } catch (SQLException e) {
            return null;
        }
    }

    @Override
    public void deliverResult(List<FileModel> data) {
        if (isStarted()) {
            super.deliverResult(data);
        }
    }

    @Override
    protected void onReset() {
        super.onReset();

        onStopLoading();

        if (mObserver != null) {
            try {
                getContext().unregisterReceiver(mObserver);
                mObserver = null;
            } catch (Exception e) {
            }
        }
    }

    public static class TestObjObserver extends BroadcastReceiver {
        AsyncTaskLoader mLoader;

        public TestObjObserver(AsyncTaskLoader loader) {
            IntentFilter filter = new IntentFilter(RELOAD);
            loader.getContext().registerReceiver(this, filter);
            mLoader = loader;
        }

        @Override
        public void onReceive(Context context, Intent intent) {
            mLoader.onContentChanged();
        }
    }

}