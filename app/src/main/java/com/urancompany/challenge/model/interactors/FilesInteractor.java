package com.urancompany.challenge.model.interactors;

import com.urancompany.challenge.model.FileModel;

import java.util.List;

/**
 * Created by Deni on 18.04.2016.
 */
public interface FilesInteractor {
    List<FileModel> getFiles();
}
