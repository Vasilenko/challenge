package com.urancompany.challenge.model;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.urancompany.challenge.model.enums.FileType;

import java.util.Date;

/**
 * Created by Deni on 18.04.2016.
 */
@DatabaseTable(tableName = "files")
public class FileModel {
    public static final String PARENT_ID = "parent_id";

    @DatabaseField(id = true)
    private int mId;

    @DatabaseField
    private String mFileName;

    @DatabaseField
    private String mIsFolder;

    @DatabaseField(columnName = PARENT_ID)
    private int mParentFolderId;

    @DatabaseField
    private Date mModDate;

    @DatabaseField(dataType = DataType.ENUM_INTEGER)
    private FileType mFileType;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean mIsOrange;

    @DatabaseField(dataType = DataType.BOOLEAN)
    private boolean mIsBlue;

    public FileModel() {
    }

    public String getFileName() {
        return mFileName;
    }

    public void setFileName(String fileName) {
        mFileName = fileName;
    }

    public String isFolder() {
        return mIsFolder;
    }

    public void setIsFolder(String isFolder) {
        mIsFolder = isFolder;
    }

    public Date getModDate() {
        return mModDate;
    }

    public void setModDate(Date modDate) {
        mModDate = modDate;
    }

    public FileType getFileType() {
        return mFileType;
    }

    public void setFileType(FileType fileType) {
        mFileType = fileType;
    }

    public boolean isOrange() {
        return mIsOrange;
    }

    public void setOrange(boolean orange) {
        mIsOrange = orange;
    }

    public boolean isBlue() {
        return mIsBlue;
    }

    public void setBlue(boolean blue) {
        mIsBlue = blue;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getParentFolderId() {
        return mParentFolderId;
    }

    public void setParentFolderId(int parentFolderId) {
        mParentFolderId = parentFolderId;
    }
}
