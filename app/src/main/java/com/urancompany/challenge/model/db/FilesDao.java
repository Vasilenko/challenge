package com.urancompany.challenge.model.db;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.support.ConnectionSource;
import com.urancompany.challenge.App;
import com.urancompany.challenge.model.FileModel;
import com.urancompany.challenge.model.loaders.FilesLoader;

import java.sql.SQLException;
import java.util.List;

public class FilesDao extends BaseDaoImpl<FileModel, Integer> {
    DatabaseHelper mDatabaseHelper;

    protected FilesDao(ConnectionSource connectionSource,
                       Class<FileModel> dataClass, DatabaseHelper databaseHelper) throws SQLException {
        super(connectionSource, dataClass);
        mDatabaseHelper = databaseHelper;
    }

    public void bulkInsert(List<FileModel> list) throws SQLException {
        if (mDatabaseHelper != null) {
            final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
            db.beginTransaction();
            try {
                for (FileModel step : list) {
                    super.createOrUpdate(step);
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
                App.getInstance().sendBroadcast(new Intent(FilesLoader.RELOAD));
            }
        }
    }

    public List<FileModel> getList() {
        QueryBuilder<FileModel, Integer> queryBuilder = queryBuilder();
        PreparedQuery<FileModel> preparedQuery;
        List<FileModel> list = null;
        try {
            preparedQuery = queryBuilder.prepare();
            list = query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public List<FileModel> getList(int parentId) {
        QueryBuilder<FileModel, Integer> queryBuilder = queryBuilder();
        try {
            queryBuilder.where().eq(FileModel.PARENT_ID, String.valueOf(parentId));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PreparedQuery<FileModel> preparedQuery;
        List<FileModel> list = null;
        try {
            preparedQuery = queryBuilder.prepare();
            list = query(preparedQuery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
}
