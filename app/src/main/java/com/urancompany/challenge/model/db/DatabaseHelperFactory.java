package com.urancompany.challenge.model.db;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.urancompany.challenge.App;

public class DatabaseHelperFactory {

    private static DatabaseHelper mDatabaseHelper;
    ;

    public synchronized static DatabaseHelper getHelper() {
        if (mDatabaseHelper == null) {
            mDatabaseHelper = OpenHelperManager.getHelper(App.getInstance(), DatabaseHelper.class);
        }
        return mDatabaseHelper;
    }

    public static void releaseHelper() {
        OpenHelperManager.releaseHelper();
        mDatabaseHelper = null;
    }
}