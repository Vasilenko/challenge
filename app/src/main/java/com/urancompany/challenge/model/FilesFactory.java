package com.urancompany.challenge.model;


import com.urancompany.challenge.model.enums.FileType;
import com.urancompany.challenge.utils.Const;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

public class FilesFactory {
    private static final Random RANDOM;
    private static final int MAX_LEVEL = 3;
    private static int ID = 1;

    static {
        RANDOM = new Random();
    }


    public static List<FileModel> generateFileModel() {
        return generateFilesModel(0, 0);
    }

    public static List<FileModel> generateFilesModel(int parentId, int currentLevel) {

        int n = generateInt(2) + 2;

        List<FileModel> fileModels = new ArrayList<>(n);

        for (int i = 0; i < n; i++) {
            FileModel fileModel = generateFileModel(ID++, parentId, currentLevel);
            fileModels.add(fileModel);

            if (fileModel.isFolder().equals(Const.YES)) {
                fileModels.addAll(generateFilesModel(fileModel.getId(), currentLevel + 1));
            }
        }

        return fileModels;
    }


    private static FileModel generateFileModel(int id, int parentId, int currentLevel) {
        FileModel fileModel = new FileModel();
        fileModel.setId(id);
        fileModel.setParentFolderId(parentId);

        fileModel.setBlue(generateInt(2) == 1);
        fileModel.setOrange(generateInt(2) == 1);
        fileModel.setFileName(generateString(generateInt(5) + 5));
        fileModel.setModDate(new Date(System.currentTimeMillis() - (long) (generateInt(1000) * 1000)));

        if (currentLevel < MAX_LEVEL)
            fileModel.setIsFolder(generateInt(2) == 1 ? Const.YES : Const.NO);
        else
            fileModel.setIsFolder(Const.NO);

        if (fileModel.isFolder().equals(Const.NO))
            fileModel.setFileType(FileType.values()[generateInt(FileType.values().length)]);


        return fileModel;
    }


    private static String generateString(int length) {
        String characters = "qwertyuiopasdfghjklzxcvbnm";
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(RANDOM.nextInt(characters.length()));
        }
        return new String(text);
    }

    private static int generateInt(int max) {
        return RANDOM.nextInt(max);
    }

}
