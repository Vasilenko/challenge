package com.urancompany.challenge.model.interactors;

import com.urancompany.challenge.model.FileModel;
import com.urancompany.challenge.model.FilesFactory;

import java.util.List;

/**
 * Created by Deni on 18.04.2016.
 */
public class RandomFilesInteractor implements FilesInteractor {

    @Override
    public List<FileModel> getFiles() {
        return FilesFactory.generateFileModel();
    }
}
