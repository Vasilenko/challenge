package com.urancompany.challenge.view.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.urancompany.challenge.App;
import com.urancompany.challenge.R;
import com.urancompany.challenge.model.FileModel;
import com.urancompany.challenge.utils.Const;

import java.util.List;

/**
 * Created by Deni on 18.04.2016.
 */
public class FilesAdapter extends BaseAdapter {
    List<FileModel> mList;

    public void setData(List<FileModel> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mList != null ? mList.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_file, parent, false);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.modified = (TextView) convertView.findViewById(R.id.modified);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.folderView = convertView.findViewById(R.id.icon_folder);
            holder.colorView = convertView.findViewById(R.id.color_view);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        FileModel data = (FileModel) getItem(position);

        if (Const.YES.equalsIgnoreCase(data.isFolder())) {
            holder.folderView.setVisibility(View.VISIBLE);
        } else {
            holder.folderView.setVisibility(View.GONE);
        }

        if (data.isBlue()) {
            holder.colorView.setVisibility(View.VISIBLE);
            holder.colorView.setBackgroundColor(App.getInstance().getResources().getColor(R.color.blue));
        } else if (data.isOrange()) {
            holder.colorView.setVisibility(View.VISIBLE);
            holder.colorView.setBackgroundColor(App.getInstance().getResources().getColor(R.color.orange));
        } else {
            holder.colorView.setVisibility(View.GONE);
        }

        holder.title.setText(data.getFileName());
        holder.modified.setText(String.valueOf(data.getModDate()));

        int image = R.drawable.ic_file;
        if (Const.YES.equals(data.isFolder())) {
            image = R.drawable.ic_folder;
        } else {
            switch (data.getFileType()) {
                case IMAGE:
                    image = R.drawable.ic_image;
                    break;
            }
        }
        holder.icon.setImageResource(image);
        return convertView;
    }

    private class ViewHolder {
        TextView title;
        TextView modified;
        View folderView;
        View colorView;
        ImageView icon;
    }
}
