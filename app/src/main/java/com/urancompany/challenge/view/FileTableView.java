package com.urancompany.challenge.view;

import com.urancompany.challenge.model.FileModel;

import java.util.List;

/**
 * Created by Deni on 18.04.2016.
 */
public interface FileTableView {
    void showData(List<FileModel> list);
}
