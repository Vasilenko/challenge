package com.urancompany.challenge.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.urancompany.challenge.R;
import com.urancompany.challenge.model.FileModel;
import com.urancompany.challenge.model.db.DatabaseHelperFactory;
import com.urancompany.challenge.presenter.FilesPresenter;
import com.urancompany.challenge.presenter.RandomFilesPresenter;
import com.urancompany.challenge.utils.Const;
import com.urancompany.challenge.view.FileTableView;
import com.urancompany.challenge.view.adapters.FilesAdapter;

import java.util.List;

public class FileTableViewActivity extends AppCompatActivity implements
        FileTableView,
        AdapterView.OnItemClickListener,
        AdapterView.OnItemLongClickListener {

    private static final String TAG = "FileTableViewActivity";

    ListView mListView;
    FilesAdapter mAdapter;
    FilesPresenter mPresenter;

    public static Intent newIntent(Context context, int parentId) {
        Intent intent = new Intent(context, FileTableViewActivity.class);
        intent.putExtra(Const.PARAM_PARENT_ID, parentId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mListView = (ListView) findViewById(R.id.list);
        mAdapter = new FilesAdapter();
        mPresenter = new RandomFilesPresenter(this, this);
        mListView.setAdapter(mAdapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPresenter.loadData(-1);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        DatabaseHelperFactory.releaseHelper();
    }

    @Override
    public void showData(List<FileModel> list) {
        mAdapter.setData(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FileModel item = (FileModel) mAdapter.getItem(position);
        if (item.isFolder().equalsIgnoreCase(Const.YES)) {
            startActivity(FileTableViewActivity.newIntent(this, item.getParentFolderId()));
        } else {
            Log.i(TAG, "This is file");
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }
}
