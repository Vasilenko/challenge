package com.urancompany.challenge;

import android.app.Application;

/**
 * Created by Deni on 18.04.2016.
 */
public class App extends Application {
    private static App sInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;
    }

    public static App getInstance() {
        return sInstance;
    }
}
